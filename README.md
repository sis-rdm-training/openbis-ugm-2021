# openBIS User Group Meeting 2021
 
## Monday, 27.09.2021
  
`09:00-09:45` **Welcome from SIS. Overview of latest developments and roadmap** – Bernd Rinn, Caterina Barillari, SIS ETH Zürich

`09:45-10:15` **Integrating imaging and OMICS data for biomedical research** – Sven Nahnsen, QBIC Tübingen
 
`10:15-10:45` **Secure handling of confidential data** – Diana Coman Schmid, SIS ETH Zürich
 
`10:45-11:00` coffee break
 
`11:00-11:15` **Structuring your research with openBIS** – Tatiana Kochetkova, Empa
 
`11:15-11:30` **To the data management and beyond: example of lab workflow in openBIS** – Nikolajs Toropovs, Empa
 
`11:30-12:00` **Digitalization of sequencing lab workflows with openBIS LIMS** – Martin Hediger, university hospital Zürich
 
`12:00-12:15` wrap up
 
`12:15-14:00` lunch break
 
`14:00-16:00` **round table discussion**


## Tuesday, 28.09.2021
 
The trainings on this day do not require any previous openBIS knowledge.
 
`09:00-12:30` **ELN user training**
 
In this course we will provide a general overview of openBIS ELN/LIMS followed by a practical training session. This course is designed for beginners and does not require any previous knowledge.


Topics covered:

* Working with Inventory of lab materials.
* Working with Inventory of lab protocols.
* Lab notebook: Registration of experiments and data upload.
 

`12:30-13:30` lunch break
 
`13:30-16:30` **Instance admin training**
 
In this course we will provide a general overview of the openBIS admin functionalities followed by a practical training session. This course is designed for openBIS Instance Admins.

**Topics covered**

* Registration of openBIS types
* Overview of openBIS property types
* Customization of ELN Settings
* Dynamic property plugins
* Entity validation plugins
* User management


## Wednesday, 29.09.2021
 
The workshops on this day require familiarity with openBIS and programming.
 
`09:00-09:30` **Introduction to development workshops – openBIS integrations overview**
 
During this introduction we will explain how openBIS integrations for labs are done, with all the moving parts and openBIS extensions involved.
 
`09:30-11:30` **Workshop 1 – creating and importing openBIS data models with Excel : the Excel master data core plugin**
 
Topics:

* How to create data models for openBIS using the XLS master data plugin.
 
`11:30-13:00` lunch break
 
`13:00-15:00` **Workshop 2 – automating data imports : the dropbox core plugin**
 
Topics:

* How to create dropboxes
* The dropbox lifecycle and rollback mechanism
* Registering openBIS entities from dropbox
* Use openBIS V3 API for search from dropbox
* Upload data from dropbox
* Parse files from dropbox
 
`15:00-15:30` coffee break
 
`15:30-17:30` **Workshop 3 – integrating with other systems : the openBIS v3 Java API**
 
 
Topics:

* openBIS V3 API Philosophy
* How to navigate the V3 API Documentation
* How to create V3 Java applications that can be used from other systems to integrate with openBIS Create entities and search entities
 
## Thursday, 30.09.2021
 
The workshops on this day require familiarity with openBIS and programming.
 
`09:00-11:00` **Workshop 4 – developing your own openBIS web apps and extending ELN-LIMS : the openBIS v3 Javascript API**

Topics:

* How to use the Javascript API from a web application with provided examples
* ELN Plugin system
* How to use the AS service plugins to create custom APIs usable from web applications
 
 
`11:00-11:15`  coffee break
 
`11:15-13:15` **Workshop 5 – working with openBIS using Python, Jupyter Lab and pyBIS**
 
 
Topics:

* connect to openBIS and browse through its content
* search, create, update and delete Samples and DataSets
* create and modify openBIS master data and ELN-LIMS settings

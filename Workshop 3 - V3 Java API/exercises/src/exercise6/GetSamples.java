package exercise6;

import ch.ethz.sis.openbis.generic.asapi.v3.IApplicationServerApi;
import common.Openbis;

public class GetSamples
{
    public static void main(String[] args)
    {
        // EXERCISE
        //
        // Get samples with the following identifiers:
        //
        //      /WORKSHOP_V3_JAVA_API/SWITZERLAND/SANTIS
        //      /WORKSHOP_V3_JAVA_API/SWITZERLAND/EIGER
        //      /WORKSHOP_V3_JAVA_API/SWITZERLAND/ZERMATT
        //
        // The samples should be fetched with their experiments (lands) and properties (elevations).
        // Print the fetched data to the console.

        IApplicationServerApi v3 = Openbis.createApplicationServerApi();
        String sessionToken = v3.login(Openbis.USER, Openbis.PASSWORD);

        // TODO

        v3.logout(sessionToken);
    }
}

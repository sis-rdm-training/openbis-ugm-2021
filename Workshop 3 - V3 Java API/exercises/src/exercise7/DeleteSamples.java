package exercise7;

import ch.ethz.sis.openbis.generic.asapi.v3.IApplicationServerApi;
import common.Openbis;

public class DeleteSamples
{
    public static void main(String[] args)
    {
        // EXERCISE
        //
        // Permanently delete sample /WORKSHOP_V3_JAVA_API/SWITZERLAND/OBERROTHORN,
        // i.e. first move the sample to Trash and then Confirm the deletion.

        IApplicationServerApi v3 = Openbis.createApplicationServerApi();
        String sessionToken = v3.login(Openbis.USER, Openbis.PASSWORD);

        // TODO

        v3.logout(sessionToken);
    }
}

package exercise7;

import java.util.List;

import ch.ethz.sis.openbis.generic.asapi.v3.IApplicationServerApi;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.deletion.id.IDeletionId;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.delete.SampleDeletionOptions;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.id.SampleIdentifier;
import common.Openbis;

public class DeleteSamplesSolution
{
    public static void main(String[] args)
    {
        // EXERCISE
        //
        // Permanently delete sample /WORKSHOP_V3_JAVA_API/SWITZERLAND/OBERROTHORN,
        // i.e. first move the sample to Trash and then Confirm the deletion.

        IApplicationServerApi v3 = Openbis.createApplicationServerApi();
        String sessionToken = v3.login(Openbis.USER, Openbis.PASSWORD);

        SampleDeletionOptions deletionOptions = new SampleDeletionOptions();
        deletionOptions.setReason("workshop exercise");

        IDeletionId deletionId =
                v3.deleteSamples(sessionToken, List.of(new SampleIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/OBERROTHORN")), deletionOptions);
        v3.confirmDeletions(sessionToken, List.of(deletionId));

        v3.logout(sessionToken);
    }
}

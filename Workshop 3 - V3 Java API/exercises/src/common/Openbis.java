package common;

import ch.ethz.sis.openbis.generic.asapi.v3.IApplicationServerApi;
import ch.ethz.sis.openbis.generic.dssapi.v3.IDataStoreServerApi;
import ch.systemsx.cisd.common.spring.HttpInvokerUtils;

public class Openbis
{

    private static final String AS_URL = "https://localhost:8443/openbis/openbis" + IApplicationServerApi.SERVICE_URL;

    private static final String DSS_URL = "https://localhost:8444/datastore_server" + IDataStoreServerApi.SERVICE_URL;

    private static final int TIMEOUT = 10000;

    public static final String USER = "admin";

    public static final String PASSWORD = "admin";

    public static IApplicationServerApi createApplicationServerApi()
    {
        return HttpInvokerUtils.createServiceStub(IApplicationServerApi.class, AS_URL, TIMEOUT);
    }

    public static IDataStoreServerApi createDataStoreServerApi()
    {
        return HttpInvokerUtils.createStreamSupportingServiceStub(IDataStoreServerApi.class, DSS_URL, TIMEOUT);
    }

}

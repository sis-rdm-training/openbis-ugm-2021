package common;

import java.util.ArrayList;
import java.util.List;

import ch.ethz.sis.openbis.generic.asapi.v3.IApplicationServerApi;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.common.operation.IOperation;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.entitytype.id.EntityTypePermId;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.experiment.create.CreateExperimentTypesOperation;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.experiment.create.CreateExperimentsOperation;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.experiment.create.ExperimentCreation;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.experiment.create.ExperimentTypeCreation;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.experiment.id.ExperimentIdentifier;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.operation.SynchronousOperationExecutionOptions;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.project.create.CreateProjectsOperation;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.project.create.ProjectCreation;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.project.id.ProjectIdentifier;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.property.DataType;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.property.create.CreatePropertyTypesOperation;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.property.create.PropertyAssignmentCreation;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.property.create.PropertyTypeCreation;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.property.id.PropertyTypePermId;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.create.CreateSampleTypesOperation;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.create.CreateSamplesOperation;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.create.SampleCreation;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.create.SampleTypeCreation;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.space.create.CreateSpacesOperation;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.space.create.SpaceCreation;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.space.id.SpacePermId;

public class TestData
{
    public static void main(String[] args)
    {
        /*
            Space: WORKSHOP_V3_JAVA_API
                Project: SWITZERLAND
                    Experiment: BERN (type: LAND)
                        Sample: BERN (type: TOWN, POPULATION property: 133115)
                        Sample: JUNGFRAU (type: SUMMIT, ELEVATION property: 4158)
                        Sample: EIGER (type: SUMMIT, ELEVATION property: 3970)
                    Experiment: OBWALDEN (type: LAND)
                        Sample: ALPNACH (type: TOWN, POPULATION property: 5242)
                        Sample: PILATUS (type: SUMMIT, ELEVATION property: 2132)
                    Experiment: ST_GALLEN (type: LAND)
                        Sample: ST_GALLEN (type: TOWN, POPULATION property: 75481)
                        Sample: SANTIS (type: SUMMIT, ELEVATION property: 2502)
                    Experiment: VALAIS (type: LAND)
                        Sample: ZERMATT (type: TOWN, POPULATION property: 5775)
                        Sample: MATTERHORN (type: SUMMIT, ELEVATION property: 4478)
        */

        List<IOperation> operations = new ArrayList<>();
        operations.add(createPropertyTypes());
        operations.add(createExperimentTypes());
        operations.add(createSampleTypes());
        operations.add(createSpaces());
        operations.add(createProjects());
        operations.add(createExperiments());
        operations.add(createSamples());

        IApplicationServerApi v3 = Openbis.createApplicationServerApi();
        String sessionToken = v3.login(Openbis.USER, Openbis.PASSWORD);
        v3.executeOperations(sessionToken, operations, new SynchronousOperationExecutionOptions());
    }

    private static IOperation createPropertyTypes()
    {
        PropertyTypeCreation creation = new PropertyTypeCreation();
        creation.setCode("ELEVATION");
        creation.setLabel("Elevation");
        creation.setDescription("Elevation");
        creation.setDataType(DataType.INTEGER);

        PropertyTypeCreation creation2 = new PropertyTypeCreation();
        creation2.setCode("POPULATION");
        creation2.setLabel("Population");
        creation2.setDescription("Population");
        creation2.setDataType(DataType.INTEGER);

        return new CreatePropertyTypesOperation(creation, creation2);
    }

    private static IOperation createExperimentTypes()
    {
        ExperimentTypeCreation creation = new ExperimentTypeCreation();
        creation.setCode("LAND");
        return new CreateExperimentTypesOperation(creation);
    }

    private static CreateSampleTypesOperation createSampleTypes()
    {
        PropertyAssignmentCreation assignmentCreation1 = new PropertyAssignmentCreation();
        assignmentCreation1.setPropertyTypeId(new PropertyTypePermId("POPULATION"));
        assignmentCreation1.setMandatory(true);

        PropertyAssignmentCreation assignmentCreation2 = new PropertyAssignmentCreation();
        assignmentCreation2.setPropertyTypeId(new PropertyTypePermId("ELEVATION"));
        assignmentCreation2.setMandatory(true);

        SampleTypeCreation typeCreation1 = new SampleTypeCreation();
        typeCreation1.setCode("TOWN");
        typeCreation1.setPropertyAssignments(List.of(assignmentCreation1));

        SampleTypeCreation typeCreation2 = new SampleTypeCreation();
        typeCreation2.setCode("SUMMIT");
        typeCreation2.setPropertyAssignments(List.of(assignmentCreation2));

        return new CreateSampleTypesOperation(typeCreation1, typeCreation2);
    }

    private static IOperation createSpaces()
    {
        SpaceCreation creation = new SpaceCreation();
        creation.setCode("WORKSHOP_V3_JAVA_API");
        return new CreateSpacesOperation(creation);
    }

    private static IOperation createProjects()
    {
        ProjectCreation creation = new ProjectCreation();
        creation.setCode("SWITZERLAND");
        creation.setSpaceId(new SpacePermId("WORKSHOP_V3_JAVA_API"));
        return new CreateProjectsOperation(creation);
    }

    private static IOperation createExperiments()
    {
        ExperimentCreation creation1 = new ExperimentCreation();
        creation1.setTypeId(new EntityTypePermId("LAND"));
        creation1.setCode("BERN");
        creation1.setProjectId(new ProjectIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND"));

        ExperimentCreation creation2 = new ExperimentCreation();
        creation2.setTypeId(new EntityTypePermId("LAND"));
        creation2.setCode("OBWALDEN");
        creation2.setProjectId(new ProjectIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND"));

        ExperimentCreation creation3 = new ExperimentCreation();
        creation3.setTypeId(new EntityTypePermId("LAND"));
        creation3.setCode("ST_GALLEN");
        creation3.setProjectId(new ProjectIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND"));

        ExperimentCreation creation4 = new ExperimentCreation();
        creation4.setTypeId(new EntityTypePermId("LAND"));
        creation4.setCode("VALAIS");
        creation4.setProjectId(new ProjectIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND"));

        return new CreateExperimentsOperation(creation1, creation2, creation3, creation4);
    }

    private static IOperation createSamples()
    {
        SampleCreation creation1 = new SampleCreation();
        creation1.setTypeId(new EntityTypePermId("TOWN"));
        creation1.setSpaceId(new SpacePermId("/WORKSHOP_V3_JAVA_API"));
        creation1.setExperimentId(new ExperimentIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/BERN"));
        creation1.setCode("BERN");
        creation1.setProperty("POPULATION", "133115");

        SampleCreation creation2 = new SampleCreation();
        creation2.setTypeId(new EntityTypePermId("SUMMIT"));
        creation2.setSpaceId(new SpacePermId("/WORKSHOP_V3_JAVA_API"));
        creation2.setExperimentId(new ExperimentIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/BERN"));
        creation2.setCode("JUNGFRAU");
        creation2.setProperty("ELEVATION", "4158");

        SampleCreation creation3 = new SampleCreation();
        creation3.setTypeId(new EntityTypePermId("SUMMIT"));
        creation3.setSpaceId(new SpacePermId("/WORKSHOP_V3_JAVA_API"));
        creation3.setExperimentId(new ExperimentIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/BERN"));
        creation3.setCode("EIGER");
        creation3.setProperty("ELEVATION", "3970");

        SampleCreation creation4 = new SampleCreation();
        creation4.setTypeId(new EntityTypePermId("TOWN"));
        creation4.setSpaceId(new SpacePermId("/WORKSHOP_V3_JAVA_API"));
        creation4.setExperimentId(new ExperimentIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/OBWALDEN"));
        creation4.setCode("ALPNACH");
        creation4.setProperty("POPULATION", "5242");

        SampleCreation creation5 = new SampleCreation();
        creation5.setTypeId(new EntityTypePermId("SUMMIT"));
        creation5.setSpaceId(new SpacePermId("/WORKSHOP_V3_JAVA_API"));
        creation5.setExperimentId(new ExperimentIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/OBWALDEN"));
        creation5.setCode("PILATUS");
        creation5.setProperty("ELEVATION", "2132");

        SampleCreation creation6 = new SampleCreation();
        creation6.setTypeId(new EntityTypePermId("TOWN"));
        creation6.setSpaceId(new SpacePermId("/WORKSHOP_V3_JAVA_API"));
        creation6.setExperimentId(new ExperimentIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/ST_GALLEN"));
        creation6.setCode("ST_GALLEN");
        creation6.setProperty("POPULATION", "75481");

        SampleCreation creation7 = new SampleCreation();
        creation7.setTypeId(new EntityTypePermId("SUMMIT"));
        creation7.setSpaceId(new SpacePermId("/WORKSHOP_V3_JAVA_API"));
        creation7.setExperimentId(new ExperimentIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/ST_GALLEN"));
        creation7.setCode("SANTIS");
        creation7.setProperty("ELEVATION", "2502");

        SampleCreation creation8 = new SampleCreation();
        creation8.setTypeId(new EntityTypePermId("TOWN"));
        creation8.setSpaceId(new SpacePermId("/WORKSHOP_V3_JAVA_API"));
        creation8.setExperimentId(new ExperimentIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/VALAIS"));
        creation8.setCode("ZERMATT");
        creation8.setProperty("POPULATION", "5775");

        SampleCreation creation9 = new SampleCreation();
        creation9.setTypeId(new EntityTypePermId("SUMMIT"));
        creation9.setSpaceId(new SpacePermId("/WORKSHOP_V3_JAVA_API"));
        creation9.setExperimentId(new ExperimentIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/VALAIS"));
        creation9.setCode("MATTERHORN");
        creation9.setProperty("ELEVATION", "4478");

        return new CreateSamplesOperation(creation1, creation2, creation3, creation4, creation5, creation6, creation7, creation8, creation9);
    }

}

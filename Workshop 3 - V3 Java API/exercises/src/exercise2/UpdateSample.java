package exercise2;

import ch.ethz.sis.openbis.generic.asapi.v3.IApplicationServerApi;
import common.Openbis;

public class UpdateSample
{
    public static void main(String[] args)
    {
        // EXERCISE
        //
        // Update sample /WORKSHOP_V3_JAVA_API/SWITZERLAND/OBERROTHORN,
        // set ELEVATION property to 3414.

        IApplicationServerApi v3 = Openbis.createApplicationServerApi();
        String sessionToken = v3.login(Openbis.USER, Openbis.PASSWORD);

        // TODO

        v3.logout(sessionToken);
    }
}
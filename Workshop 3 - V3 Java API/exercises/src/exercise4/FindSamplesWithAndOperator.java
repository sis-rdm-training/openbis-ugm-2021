package exercise4;

import ch.ethz.sis.openbis.generic.asapi.v3.IApplicationServerApi;
import common.Openbis;

public class FindSamplesWithAndOperator
{
    public static void main(String[] args)
    {
        // EXERCISE
        //
        // Find all samples that have:
        //
        //      project = /WORKSHOP_V3_JAVA_API/SWITZERLAND
        //      AND type = SUMMIT
        //      AND ELEVATION property greater than 4000

        IApplicationServerApi v3 = Openbis.createApplicationServerApi();
        String sessionToken = v3.login(Openbis.USER, Openbis.PASSWORD);

        // TODO

        v3.logout(sessionToken);
    }
}
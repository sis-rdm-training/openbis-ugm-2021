package exercise2;

import java.util.List;

import ch.ethz.sis.openbis.generic.asapi.v3.IApplicationServerApi;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.id.SampleIdentifier;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.update.SampleUpdate;
import common.Openbis;

public class UpdateSampleSolution
{
    public static void main(String[] args)
    {
        // EXERCISE
        //
        // Update sample /WORKSHOP_V3_JAVA_API/SWITZERLAND/OBERROTHORN,
        // set ELEVATION property to 3414.

        IApplicationServerApi v3 = Openbis.createApplicationServerApi();
        String sessionToken = v3.login(Openbis.USER, Openbis.PASSWORD);

        SampleUpdate sampleUpdate = new SampleUpdate();
        sampleUpdate.setSampleId(new SampleIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/OBERROTHORN"));
        sampleUpdate.setProperty("ELEVATION", "3414");

        v3.updateSamples(sessionToken, List.of(sampleUpdate));

        v3.logout(sessionToken);
    }
}
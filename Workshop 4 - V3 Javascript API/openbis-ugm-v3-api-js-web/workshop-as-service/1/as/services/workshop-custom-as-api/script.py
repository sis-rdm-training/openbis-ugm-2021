def process(context, parameters):
	method = parameters.get("method")
	result = None
	try:
		if method == "echo":
			message = parameters.get("message")
			result = {
				"status" : "OK",
				"result" : message
			}
	except Exception, value:
		result = str(value)
	return result